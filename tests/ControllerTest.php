<?php

use Boozt\Dashboard\Controller\CustomerController;
use Boozt\Dashboard\Controller\OrderController;
use Boozt\Dashboard\Controller\OrderItemController;

class ControllerTest extends PHPUnit\Framework\TestCase
{

    public function defineEnvironment($env)
    {
        if (defined("ENV")) {
            return;
        }
        define("ENV", $env);
    }

    public function testOrderGetAmount()
    {
        $this->defineEnvironment("dev");
        $controller = new OrderController('{ "start": "2019-01-01", "end": "2019-01-05" }');
        $this->assertInternalType('string', $controller->getAmount());
    }

    public function testOrderGetRevenue()
    {
        $this->defineEnvironment("dev");
        $controller = new OrderItemController('{ "start": "2019-01-01", "end": "2019-01-05" }');
        $this->assertInternalType('string', $controller->getRevenue());
    }

    public function testCustomerGetAmount()
    {
        $this->defineEnvironment("dev");
        $controller = new CustomerController('{ "start": "2019-01-01", "end": "2019-01-05" }');
        $this->assertInternalType('string', $controller->getAmount());
    }

    public function testCustomerGetAmountDaily()
    {
        $this->defineEnvironment("dev");
        $controller = new CustomerController('{ "start": "2019-01-01", "end": "2019-01-05" }');
        $this->assertInternalType('array', $controller->getAmountDaily());
    }

    public function testOrderGetAmountDaily()
    {
        $this->defineEnvironment("dev");
        $controller = new OrderController('{ "start": "2019-01-01", "end": "2019-01-05" }');
        $this->assertInternalType('array', $controller->getAmountDaily());
    }

}
