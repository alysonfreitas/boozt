<?php

$autoload = __DIR__ . '/vendor/autoload.php';
if(!file_exists($autoload)){
    die('You must run "composer install" command');
}
require_once $autoload;

use Boozt\Dashboard\Core\HttpHandler;

define("ENV", getenv("ENVIRONMENT") != null ? getenv("ENVIRONMENT") : "dev");

$handler = new HttpHandler();
$handler->handle();
