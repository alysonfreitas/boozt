# Boozt - Project environment (using wamp64)
- Apache version 2.4.33
- PHP version 7.2.4
- MySQL version 5.7.21
- Composer 4.9

*This project relies on apache RewriteEngine as defined on .htaccess*

**Step 1:**
Clone or extract files under "/www" you shall have something like: "/www/boozt"

**Step 2:**
Using cmd, run "composer install" inside "/www/boozt" folder

**Step 3:**
Create database using the exported script under "/www/boozt/sql/mysql-db-dump-boozt.sql"

**Step 4:**
Configure your environment under "/www/boozt/src/config/Config.php"
You must change the constants to match your database settings

**Optional:**
You can run unit test using the following command: "vendor/bin/phpunit"

The project should be running at: http://localhost/boozt