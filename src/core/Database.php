<?php
namespace Boozt\Dashboard\Core;

use Boozt\Dashboard\Config\Config;
use PDO;

class Database
{

    private static $instance;
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function connect()
    {
        try {
            $config = Config::ENVIRONMENT[ENV];
            $dbh = new PDO('mysql:host=' . $config["DB_HOST"] . ';dbname=' . $config["DB_NAME"], $config["DB_USER"], $config["DB_PASSWORD"]);
            return $dbh;
        } catch (PDOException $e) {
            die("Error!: " . $e->getMessage());
        }
    }
}
