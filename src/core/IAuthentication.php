<?php
namespace Boozt\Dashboard\Core;

interface IAuthentication
{
    public function requireAuth();
}
