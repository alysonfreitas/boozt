<?php
namespace Boozt\Dashboard\Core;

use Boozt\Dashboard\Config\Config;
use Boozt\Dashboard\Core\IAuthentication;

class HttpHandler
{
    private $config;

    public function __construct()
    {
        $this->config = Config::ENVIRONMENT[ENV];
    }

    public function handle()
    {

        $requestPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $requestPath = str_replace($this->config["URL_BASE"], '', $requestPath);

        if (strpos($requestPath, 'api/') !== false) {
            header('Content-Type: application/json');
            echo $this->handleApiResult($requestPath);
            return;
        }
        echo $this->handleFileResult($requestPath);
    }

    public function handleApiResult($requestPath)
    {
        $namespace = $this->config["CONTROLLER_NAMESPACE"];

        $api = explode('/', $requestPath);
        $length = count($api);

        $controller = $namespace . $api[2] . "Controller";
        $method = $length > 3 ? $api[3] : '';
        $param = $length > 4 ? $api[4] : null;

        if (!class_exists($controller)) {
            header("HTTP/1.1 500 Internal Server Error");
            return "Controller not found";
        }

        $instance = new $controller(file_get_contents('php://input'));

        if ($instance instanceof IAuthentication) {
            if (!$this->handleAuthentication($instance)) {
                header("HTTP/1.1 401 Unauthorized");
                return "Unauthorized";
            }
        }

        $response = $length > 4 ? $instance->$method($param) : $instance->$method();
        return json_encode($response);
    }

    public function handleFileResult($requestPath)
    {
        $requestFile = $this->config["PUBLIC_DIR"] . $requestPath;
        if (file_exists($requestFile) && !is_dir($requestFile)) {
            return $this->fixBase(file_get_contents($requestFile));
        }
        return $this->fixBase(file_get_contents($this->config["PUBLIC_DIR"] . DIRECTORY_SEPARATOR . $this->config["DEFAULT_FILE"]));
    }

    public function fixBase($file)
    {
        return str_replace('[::CONFIG-URL-BASE::]', '/' . $this->config["URL_BASE"], $file);
    }

    public function handleAuthentication($instance)
    {
        if ($instance->requireAuth()) {
            $auth = null;
            foreach (getallheaders() as $name => $value) {
                if ($name == "Authorization") {
                    $auth = $value;
                }
            }
            return $auth != null;
        }
        return true;
    }
}
