<?php
namespace Boozt\Dashboard\Core;

use Boozt\Dashboard\Core\Database;

abstract class Model
{

    public $db = null;

    public function __construct()
    {
        $database = Database::getInstance();
        $this->db = $database->connect();
    }

}
