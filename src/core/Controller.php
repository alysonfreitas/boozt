<?php
namespace Boozt\Dashboard\Core;

abstract class Controller
{

    private $post = [];

    public function __construct($arg)
    {
        $this->post = json_decode($arg, true);
    }

    public function getParam($param, $default)
    {
        if (isset($this->post[$param])) {
            return $this->post[$param];
        }
        return $default;
    }

}
