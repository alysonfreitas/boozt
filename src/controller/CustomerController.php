<?php
namespace Boozt\Dashboard\Controller;

use Boozt\Dashboard\Core\Controller;
use Boozt\Dashboard\Core\IAuthentication;
use Boozt\Dashboard\Model\CustomerModel;

class CustomerController extends Controller implements IAuthentication
{

    /**
     * returns if requires authentication
     */
    public function requireAuth()
    {
        return false;
    }

    /**
     * returns amount of customers in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     * this method must be requested as POST and parameters send as JSON
     */
    public function getAmount()
    {
        $start = $this->getParam("start", date('Y-m-d'));
        $end = $this->getParam("end", date('Y-m-d'));
        $model = new CustomerModel();
        return $model->getAmount($start, $end);
    }

    /**
     * returns amount of customers by day in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     * this method must be requested as POST and parameters send as JSON
     */
    public function getAmountDaily()
    {
        $start = $this->getParam("start", date('Y-m-d'));
        $end = $this->getParam("end", date('Y-m-d'));
        $model = new CustomerModel();
        return $model->getAmountDaily($start, $end);
    }
}
