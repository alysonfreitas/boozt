<?php
namespace Boozt\Dashboard\Controller;

use Boozt\Dashboard\Core\Controller;
use Boozt\Dashboard\Core\IAuthentication;
use Boozt\Dashboard\Model\OrderItemModel;

class OrderItemController extends Controller implements IAuthentication
{

    /**
     * returns if requires authentication
     */
    public function requireAuth()
    {
        return false;
    }

    /**
     * returns revenue in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     * this method must be requested as POST and parameters send as JSON
     */
    public function getRevenue()
    {
        $start = $this->getParam("start", date('Y-m-d'));
        $end = $this->getParam("end", date('Y-m-d'));
        $model = new OrderItemModel();
        return $model->getRevenue($start, $end);
    }

}
