app.config(['$locationProvider', '$stateProvider', '$urlRouterProvider', function ($locationProvider, $stateProvider, $urlRouterProvider) {

    $locationProvider.html5Mode(true).hashPrefix('!');

    // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
    $urlRouterProvider
        .when('/dashboard', '/')
        .otherwise('/');

    $stateProvider.state("dashboard", {
        url: "/",
        templateUrl: 'app/templates/dashboard.html',
        controller: 'dashboard',
    });
}
]);
