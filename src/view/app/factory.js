angular.module('app').factory('DateFormat', function () {
    return {
        parseDate: function (input) {
            var parts = input.split('-');
            return new Date(parts[0], parts[1] - 1, parts[2]);
        },
        parseDateUTC: function (input) {
            var parts = input.split('-');
            return Date.UTC(parts[0], parts[1] - 1, parts[2]);
        },
        formatDate: function (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }
    }
});