var controller = function ($scope, $http, $rootScope, DateFormat) {

    $scope.filter = {
        start: DateFormat.formatDate(new Date(new Date().getFullYear(), new Date().getMonth(), 1)),
        end: DateFormat.formatDate(new Date())
    }

    $scope.stats = {
        order: {},
        customer: {}
    }

    $scope.chart = {
        order: [],
        customer: [],
        instance: null
    }

    $scope.load = function () {

        if ($scope.filter.start.length == 0) {
            toastr.warning('Start date is required');
            return;
        }

        if ($scope.filter.end.length == 0) {
            toastr.warning('End date is required');
            return;
        }

        if (DateFormat.parseDate($scope.filter.start).getTime() > DateFormat.parseDate($scope.filter.end).getTime()) {
            toastr.warning('Start date must to be lower than end date');
            return;
        }

        // stats load
        $scope.getRevenue();
        $scope.getOrderAmount();
        $scope.getCustomerAmount();

        // chart load
        $scope.getOrderAmountDaily();
        $scope.getCustomerAmountDaily();
    }

    $scope.getRevenue = function () {
        $rootScope.loading += 1;
        $http({
            method: "POST",
            url: "api/orderitem/getRevenue",
            data: $scope.filter
        }).then(function mySuccess(response) {
            $rootScope.loading -= 1;
            $scope.stats.revenue = response.data;
        }, function myError(response) {
            $rootScope.loading -= 1;
            toastr.error(response.data);
        });
    }

    $scope.getOrderAmount = function () {
        $rootScope.loading += 1;
        $http({
            method: "POST",
            url: "api/order/getAmount",
            data: $scope.filter
        }).then(function mySuccess(response) {
            $rootScope.loading -= 1;
            $scope.stats.order.amount = response.data;
        }, function myError(response) {
            $rootScope.loading -= 1;
            toastr.error(response.data);
        });
    }

    $scope.getCustomerAmount = function () {
        $rootScope.loading += 1;
        $http({
            method: "POST",
            url: "api/customer/getAmount",
            data: $scope.filter
        }).then(function mySuccess(response) {
            $rootScope.loading -= 1;
            $scope.stats.customer.amount = response.data;
        }, function myError(response) {
            $rootScope.loading -= 1;
            toastr.error(response.data);
        });
    }

    $scope.getOrderAmountDaily = function () {
        $rootScope.loading += 1;
        $http({
            method: "POST",
            url: "api/order/getAmountDaily",
            data: $scope.filter
        }).then(function mySuccess(response) {
            $rootScope.loading -= 1;
            $scope.chart.order = response.data;
            $scope.updateChart();
        }, function myError(response) {
            $rootScope.loading -= 1;
            toastr.error(response.data);
        });
    }

    $scope.getCustomerAmountDaily = function () {
        $rootScope.loading += 1;
        $http({
            method: "POST",
            url: "api/customer/getAmountDaily",
            data: $scope.filter
        }).then(function mySuccess(response) {
            $rootScope.loading -= 1;
            $scope.chart.customer = response.data;
            $scope.updateChart();
        }, function myError(response) {
            $rootScope.loading -= 1;
            toastr.error(response.data);
        });
    }

    $scope.updateChart = function () {
        var options = {
            title: {
                text: 'Customers and Orders'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: { // don't display the dummy year
                    month: '%e. %b',
                    year: '%b'
                },
                title: {
                    text: 'Date'
                }
            },
            yAxis: {
                title: {
                    text: 'Amount'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                spline: {
                    marker: {
                        enabled: true
                    }
                }
            },
            series: [],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        };

        if (Array.isArray($scope.chart.order)) {
            var data = $scope.chart.order.map(function (r) { return [DateFormat.parseDateUTC(r.date), parseInt(r.value)] });
            options.series.push({
                name: 'Orders',
                data: data
            })
        }

        if (Array.isArray($scope.chart.customer)) {
            var data = $scope.chart.customer.map(function (r) { return [DateFormat.parseDateUTC(r.date), parseInt(r.value)] });
            options.series.push({
                name: 'Customers',
                data: data
            })
        }

        if ($scope.chart.instance != null) {
            $scope.chart.instance.destroy();
        }

        $scope.chart.instance = Highcharts.chart('chart-container', options);
    }
}

angular.module('app').controller('dashboard', controller);