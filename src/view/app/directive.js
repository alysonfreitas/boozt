angular.module('app').directive('datepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: 'yyyy-mm-dd',
                language: 'en-CA',
                pickTime: false,
                orientation: "bottom"
            }).on('changeDate', function (e) {
                ngModelCtrl.$setViewValue(e.date.toLocaleDateString());
                scope.$apply();
            });

            element.bind('keyup', function () {
                if (element.val() == '' || element.val() == '____-__-__') {
                    element.val('').datepicker('update');
                    ngModelCtrl.$setViewValue('');
                    scope.$apply();
                }
            });
        }
    };
});