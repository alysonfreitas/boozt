<?php
namespace Boozt\Dashboard\Config;

class Config
{
    const ENVIRONMENT = array(
        "dev" => array(
            "DB_HOST" => "localhost",
            "DB_NAME" => "boozt",
            "DB_USER" => "root",
            "DB_PASSWORD" => "root",
            "URL_BASE" => "boozt/",
            "PUBLIC_DIR" => "src/view",
            "DEFAULT_FILE" => "index.html",
            "CONTROLLER_NAMESPACE" => "Boozt\\Dashboard\\Controller\\",
        ),
    );
}
