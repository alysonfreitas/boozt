<?php
namespace Boozt\Dashboard\Model;

use Boozt\Dashboard\Core\Model;
use PDO;

class OrderModel extends Model
{

    /**
     * returns amount of orders in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     */
    public function getAmount($start, $end)
    {
        $query = 'select count(id) amount from `order`
        where purchase_date between :start and :end;';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':start', $start, PDO::PARAM_STR);
        $sth->bindParam(':end', $end, PDO::PARAM_STR);
        $sth->execute();

        $result = $sth->fetchColumn();
        return $result;
    }

    /**
     * returns amount of orders by day in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     */
    public function getAmountDaily($start, $end)
    {
        $query = 'select sum(amount) value, purchase_date date from (
            select count(id) amount, purchase_date from `order`
            where purchase_date between :start and :end
            group by purchase_date) as t group by purchase_date;';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':start', $start, PDO::PARAM_STR);
        $sth->bindParam(':end', $end, PDO::PARAM_STR);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_OBJ);
    }

}
