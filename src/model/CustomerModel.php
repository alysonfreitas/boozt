<?php
namespace Boozt\Dashboard\Model;

use Boozt\Dashboard\Core\Model;
use PDO;

class CustomerModel extends Model
{

    /**
     * returns amount of customers in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     */
    public function getAmount($start, $end)
    {
        $query = 'select count(costumers) from (
            select distinct c.id costumers from  `order` as o
            join `customer` as c on o.id_customer = c.id
            where `purchase_date` between :start and :end
            group by c.id) as t;';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':start', $start, PDO::PARAM_STR);
        $sth->bindParam(':end', $end, PDO::PARAM_STR);
        $sth->execute();

        $result = $sth->fetchColumn();
        return $result;
    }

    /**
     * returns amount of customers by day in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     */
    public function getAmountDaily($start, $end)
    {
        $query = 'select count(costumers) value, purchase_date date from (
            select distinct c.id costumers, o.purchase_date from  `order` as o
            join `customer` as c on o.id_customer = c.id
            where `purchase_date` between :start and :end
            group by c.id) as t group by purchase_date;';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':start', $start, PDO::PARAM_STR);
        $sth->bindParam(':end', $end, PDO::PARAM_STR);
        $sth->execute();

        return $sth->fetchAll(PDO::FETCH_OBJ);
    }

}
