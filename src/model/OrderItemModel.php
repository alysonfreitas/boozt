<?php
namespace Boozt\Dashboard\Model;

use Boozt\Dashboard\Core\Model;
use PDO;

class OrderItemModel extends Model
{

    /**
     * returns revenue in given period
     * expects as parameter "start" and "end" (date, format: 'Y-m-d')
     */
    public function getRevenue($start, $end)
    {
        $query = 'select SUM(i.price * i.quantity) revenue from `order_items` as i
        join `order` as o on o.id = i.id_order
        where `purchase_date` between :start and :end;';

        $sth = $this->db->prepare($query);
        $sth->bindParam(':start', $start, PDO::PARAM_STR);
        $sth->bindParam(':end', $end, PDO::PARAM_STR);
        $sth->execute();

        $result = $sth->fetchColumn();
        return $result;
    }
}
